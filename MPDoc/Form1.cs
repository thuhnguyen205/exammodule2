﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ArchiveExtract
{
    public partial class Form1 : Form
    {
        public Form1() {
            InitializeComponent();
		}

		private void tsmiQuit_Click(object sender, EventArgs e) {
			this.Close();
		}

        private void tsmiOpen_Click(object sender, EventArgs e)
        {
            DialogResult drResult = ofdArchive.ShowDialog();

            if (drResult == DialogResult.OK)
            {
                FileStream fsArchiveFile = File.Open(ofdArchive.FileName, FileMode.Open, FileAccess.Read);
                Archive(fsArchiveFile);
                vDisplayPage();
            }
        }

        private void Archive(FileStream fsArchiveFile)
        {
            fsArchiveFile.Seek(-1, SeekOrigin.End);
            int iNumPages = fsArchiveFile.ReadByte();
            int iOffset = iNumPages * 8 + 1;
            fsArchiveFile.Seek(-iOffset, SeekOrigin.End);
            int iNumBytesIndex = iOffset - 1;
            for (int iRepeat = 0; iRepeat < iNumPages; iRepeat++)
            {
                byte[] byBuffer = new byte[4];

                fsArchiveFile.Read(byBuffer, 0, byBuffer.Length);
                int iOffsetValue = byBuffer[0] + byBuffer[1] * 256 + byBuffer[2] * (65536) + byBuffer[3] * (16777216);

                fsArchiveFile.Read(byBuffer, 0, byBuffer.Length);
                int iLengthValue = byBuffer[0] + byBuffer[1] * 256 + byBuffer[2] * (65536) + byBuffer[3] * (16777216);

                int iNewRowIndex = dgvPageInfo.Rows.Add();
                DataGridViewRow dgvrNew = dgvPageInfo.Rows[iNewRowIndex];
                string strPage = "Page " + (iRepeat + 1);
                dgvrNew.Cells["PageNum"].Value = strPage;
                dgvrNew.Cells["Length"].Value = iLengthValue;
                dgvrNew.Cells["Offset"].Value = iOffsetValue;
            }
            fsArchiveFile.Close();
        }

        private void vDisplayPage()
        {
            if (dgvPageInfo.SelectedRows.Count > 0)
            {
                FileStream fsArchiveFile = File.Open(ofdArchive.FileName, FileMode.Open, FileAccess.Read);
                fsArchiveFile.Seek(0, SeekOrigin.Begin);
                DataGridViewRow dgvrRow = dgvPageInfo.SelectedRows[0];
                int iOffset = (int)dgvrRow.Cells["Offset"].Value;
                fsArchiveFile.Seek(iOffset, SeekOrigin.Begin);
                int iLength = (int)dgvrRow.Cells["Length"].Value;
                int iNumBytesRemain = iLength;
                while (iNumBytesRemain > 0)
                {
                    int iNumBytesRead = Math.Min(16, iNumBytesRemain);
                    byte[] byBuffer = new byte[iNumBytesRead];
                    fsArchiveFile.Read(byBuffer, 0, byBuffer.Length);
                    string strNewChars = Encoding.UTF8.GetString(byBuffer, 0, byBuffer.Length);
                    rtbPageText.Text += strNewChars;
                    iNumBytesRemain -= iNumBytesRead;
                }

                fsArchiveFile.Close();
            }
            
        }
        private void dgvPageInfo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            rtbPageText.Clear();
            vDisplayPage();
        }
    }
}
